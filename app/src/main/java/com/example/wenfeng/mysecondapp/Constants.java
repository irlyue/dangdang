package com.example.wenfeng.mysecondapp;

public class Constants {
    public interface ACTION{
        public static String START_FOREGROUND_ACTION = "start foreground";
        public static String STOP_FOREGROUND_ACTION = "stop foreground";
        public static String MAIN_ACTION = "main";
        public static String INIT_ACTION = "initialization";
    }
    public interface NOTIFICATION_ID{
        public static int FOREGROUND_SERVICE = 10;
    }
}
